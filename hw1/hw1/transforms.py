import torch
import numpy as np


class TensorView(object):
    """
    A transform that returns a new view of a tensor.
    """

    def __init__(self, *view_dims):
        self.view_dims = view_dims

    def __call__(self, tensor: torch.Tensor):
        # TODO: Use Tensor.view() to implement the transform.
        return tensor.view(self.view_dims)


class InvertColors(object):
    """
    Inverts colors in an image given as a tensor.
    """

    def __call__(self, x: torch.Tensor):
        """
        :param x: A tensor of shape (C,H,W) representing an image.
        :return: The image with inverted colors.
        """
        # TODO: Invert the colors of the input image.
        # t = x.clone()
        # for i in range(t.shape[0]):
        #     for j in range(t.shape[1]):
        #         for k in range(t.shape[2]):
        #             t[i, j, k] = 255 - t[i, j, k].item()
        return 1.-x


class FlipUpDown(object):
    def __call__(self, x: torch.Tensor):
        """
        :param x: A tensor of shape (C,H,W) representing an image.
        :return: The image, flipped around the horizontal axis.
        """
        # TODO: Flip the input image so that up is down.
        r_np_arr = np.flip(x.numpy(), axis=1).copy()
        return torch.from_numpy(r_np_arr)


class BiasTrick(object):
    """
    A transform that applies the "bias trick": Prepends an element equal to
    1 to each sample in a given tensor.
    """

    def __call__(self, x: torch.Tensor):
        """
        :param x: A pytorch tensor of shape (D,) or (N1,...Nk, D).
        We assume D is the number of features and the N's are extra
        dimensions. E.g. shape (N,D) for N samples of D features;
        shape (D,) or (1, D) for one sample of D features.
        :return: A tensor with D+1 features, where a '1' was prepended to
        each sample's feature dimension.
        """
        assert x.dim() > 0, "Scalars not supported"

        # TODO:
        #  Add a 1 at the beginning of the given tensor's feature dimension.
        #  Hint: See torch.cat().
        torch_ones = torch.ones(*x.shape[:-1], 1, dtype=x.dtype)
        dim = x.dim() - 1
        xb = torch.cat((torch_ones, x), dim)
        return xb

        # ones_tensor = torch.ones((*x.shape[:-1], 1), dtype=x.dtype)
        # return torch.cat((ones_tensor, x), dim=x.dim() - 1)
