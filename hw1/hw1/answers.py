r"""
Use this module to write your answers to the questions in the notebook.

Note: Inside the answer strings you can use Markdown format and also LaTeX
math (delimited with $$).
"""

# ==============
# Part 2 answers

part2_q1 = r"""
**Answer:**

The increase of $k$ does not lead to better generalization of unseen data. Mark $n$ as the number of
training examples and let us consider $k = n$. This will lead to all unseen examples being labelled according to the
most common label in the training set, and since the training set is fixed, all unseen data will have the same label,
meaning there is almost no generalization (This is under-fitting).
Now, if we consider $k = 1$, We see that each new example will be labelled the same as it's closest neighbor.
This means that each noisy example in the training set will hurt all examples which are to close to it, 
leading to, again, almost no generalization (this is over-fitting).
Since we assume there is an optimal $k$ for the model, there must exist an optimal $1 <= k <= n$ which leads to the best
generalization, and we must select it in such a way that it tries to minimize the under-fitting and over-fitting. 
In our case, the best $k$ was 3.

"""

part2_q2 = r"""
**Answer:**

Let's refer to each of the given methods:

1. If we use the training set as a whole to evaluate the accuracy and train the model, we effectively give the model no
new data in the validation phase, because we are seeing the same data, and will thus conclude that $k=1$ is the optimal
value, since it leads to a classifier which is consistent with it's training set. This means that the model is unable
to generalize, and his highly overfitted to the training data.

2. Generally, testing different models on the entire train/test sets can lead the good results, but can take a long time
if these data sets are too big. In this case, K-fold CV will be better in a sense that it will be faster, while
not sacrificing too much accuracy, since we are still selecting (possibly random) subsets of our train/test sets.
Another point to consider is that training on the entire train/test set can lead to over-fitting of the model while
using K-fold CV will reduce the fitting, since we are looking at $k$ (somewhat) independent sets.

"""

# ==============

# ==============
# Part 3 answers

part3_q1 = r"""
**Your answer:**

The selection of $\Delta > 0$ is arbitrary for the SVM loss $L(\mat{W})$ as it is defined above since it enforces 
some distance between the selected class score and the rest of the classes. This is useful in the case where we want
to give some meaning to these distances. In our case, we only want the classified label for each example, and 
therefore we don't care about the distance itself, and can choose any arbitrary $\Delta > 0$.

"""

part3_q2 = r"""
**Answer:**

We can slightly see "shapes" emerge in the 28x28 patterns we've extracted from the weights, which resemble the digits.
Thus, we can determine that the model is trying to learn some generalized representation of the outlines of a digit.
The areas in which the outline of a given handwritten digit should pass are given more dominant values for the weights.
This might explain some of the classification errors, as this model greatly depends on parameters of the handwriting
such as the size of the strokes and others. Generally, number which resemble each other or come from greatly different
handwriting styles can be mis-interpreted.  
This is similar to the KNN classifier in a sense that we are adjusting the weights according to the previous examples
and the current batch, using SGD. the difference is, that in KNN we are only looking at the K nearest neighbors and
therefore find it hard to generalize the model to new, unseen data. In the linear classifier, we are effectively
using the weights to 'remember' the patterns from **all** previous images, thus constructing a more generic pattern
which we can see graphically.

The classification errors can be explained as overfitting, similar to KNN where $k$ approaches large numbers. This means
that certain areas of the image (weights) can become 'biased' towards the training set, as we see, for example, a rotated
6 being misinterpreted as a 4 because we did not see enough rotated 6-es.

"""

part3_q3 = r"""
**Answer:**

1. From the graphs we can conclude that our learning rate is fairly good, maybe a bit on the high side.
We see the the model is slowly converging to a loss functions which is close to 0. If the learning rate was too high 
we would see more spikes in the graph, since we have a higher change of 'overshooting' the extrema points. 
If the learning rate was too low, the model might have a higher chance of converging to a good extremum, 
but the number of epochs needed to minimize the loss would likely increase and cause the learning process to either
be to slow or produce worse loss values. When we increased weight decay, we saw that the graph had much more spikes,
and thus concluded that the model was overshooting most of the time.

2. We can see from the validation set graph that our model is highly overfitted to the training set. The reason is, that
the graph produces strictly lower accuracies for almost all new examples it sees in the validation set. This 
probably happened because weight decay was too high, causing the weights to adjust too tightly to the training set,
which led to high validation errors. When we tried to slightly decrease the weight decay, we noticed that the graph of
test set accuracy is much more close to the training set accuracy, which means that there is less difference between 
training examples and validation ones, and the model better generalizes on unseen data.

"""

# ==============

# ==============
# Part 4 answers

part4_q1 = r"""
**Answer:**

We can see that by tuning the hyper-parameters using $k$-fold CV, we are able to achieve a smaller error
(higher accuracy) than the method of selecting the top 5 correlated features. We can thus say that tuning the
hyper-parameters is more significant in this case, than discarding low-correlation features. This might be due to a
number of reasons:
1. Some feature might not be linearly-dependant, but be squarely-dependant, i.e there might exist a pair of features
$z, w$ such that $w = z^2$ and $corr(z, w) << 1$ and they might not be selected, even though they are related.
2. Selecting the top $n$ features means that $n$ itself is also a hyper-parameter of the model, and therefore
arbitrarily selecting $n = 5$ would likely produce worse results than any method of hyper-parameter tuning such as CV.

Since we are looking at a linear regression pattern and plotting $y - \hat{y}$ against $y$, we expect that ideally,
the pattern will be a linear line $y - \hat{y} = 0$, meaning that our linear regressor perfectly predicts the target
function $\hat{y}$ 

"""

part4_q2 = r"""
**Your answer:**

1. For the regularization term $\lambda$, we wish to find an optimal parameter from a relatively wide range, without
the need to train the model using CV many times. This means that we can sample a big range without sacrificing the
runtime overhead too much (as we will see, even for logarithmic scale we still have to train the model for almost 200
times). In addition, training with logarithmic scale will probably be better than uniformly distributing values over
a large range, since then we would get values which vastly differ and propose a better
sample of the value range we are working on. We check in this way small values and larger values together, 
and maybe the large difference between them will cause a better results.

2. The model fitted to data (with the parameters as given, and not including the final fit on the entire training set):
$(\mathrm{k\ folds} \cdot |\mathrm{degree\ range}| \cdot |\mathrm{lambda\ range}|)$ times. In our case, 
$\mathrm{k\ folds} = 3$, $\mathrm{degree\ range} = 3$, $\mathrm{lambda\ range} = 20$, so our model fitted 180 times 
in total. 

"""

# ==============
