import numpy as np
import torch
from torch import Tensor
from torch.utils.data import Dataset, DataLoader

import cs236781.dataloader_utils as dataloader_utils
from . import dataloaders


class KNNClassifier(object):
    def __init__(self, k):
        self.k = k
        self.x_train = None
        self.y_train = None
        self.n_classes = None

    def train(self, dl_train: DataLoader):
        """
        Trains the KNN model. KNN training is memorizing the training data.
        Or, equivalently, the model parameters are the training data itself.
        :param dl_train: A DataLoader with labeled training sample (should
            return tuples).
        :return: self
        """

        x_train, y_train = dataloader_utils.flatten(dl_train)
        n_classes = len(np.unique(y_train))
        self.x_train = x_train
        self.y_train = y_train
        self.n_classes = n_classes
        return self

    def predict(self, x_test: Tensor):
        """
        Predict the most likely class for each sample in a given tensor.
        :param x_test: Tensor of shape (N,D) where N is the number of samples.
        :return: A tensor of shape (N,) containing the predicted classes.
        """

        # Calculate distances between training and test samples
        dist_matrix = l2_dist(self.x_train, x_test)
        n_test = x_test.shape[0]
        y_pred = torch.zeros(n_test, dtype=torch.int64)

        for i in range(n_test):
            col = dist_matrix.transpose(0, 1)[i]
            k_nearest_neighbors, k_idx = (torch.topk(col, self.k, largest=False))
            best_k = self.y_train[k_idx]
            hist, _ = np.histogram(best_k, self.n_classes, range=(0, self.n_classes))
            best_label = np.argmax(hist)
            y_pred[i] = torch.tensor(best_label)

        return y_pred


def l2_dist(x1: Tensor, x2: Tensor):
    """
    Calculates the L2 (euclidean) distance between each sample in x1 to each
    sample in x2.
    :param x1: First samples matrix, a tensor of shape (N1, D).
    :param x2: Second samples matrix, a tensor of shape (N2, D).
    :return: A distance matrix of shape (N1, N2) where the entry i, j
    represents the distance between x1 sample i and x2 sample j.
    """

    # TODO:
    #  Implement L2-distance calculation efficiently as possible.
    #  Notes:
    #  - Use only basic pytorch tensor operations, no external code.
    #  - Solution must be a fully vectorized implementation, i.e. use NO
    #    explicit loops (yes, list comprehensions are also explicit loops).
    #    Hint: Open the expression (a-b)^2. Use broadcasting semantics to
    #    combine the three terms efficiently.

    x2_tran = x2.transpose(0, 1)
    n1, n2 = x1.size(), x2_tran.size()
    x1_pow = (x1 ** 2)
    x2_tran_pow = (x2_tran ** 2)
    t1_t2 = x1.mm(x2_tran)
    t1 = x1_pow.mm(torch.ones(list(n2)))
    t2 = torch.ones(n1).mm(x2_tran_pow)
    dists_res = t1 - 2 * t1_t2 + t2
    dists = torch.sqrt(dists_res)
    return dists


def accuracy(y: Tensor, y_pred: Tensor):
    """
    Calculate prediction accuracy: the fraction of predictions in that are
    equal to the ground truth.
    :param y: Ground truth tensor of shape (N,)
    :param y_pred: Predictions vector of shape (N,)
    :return: The prediction accuracy as a fraction.
    """
    assert y.shape == y_pred.shape
    assert y.dim() == 1

    # TODO: Calculate prediction accuracy. Don't use an explicit loop.
    res = np.array(y == y_pred).sum()
    accuracy = (res / y.size())[0]
    return accuracy


def find_best_k(ds_train: Dataset, k_choices, num_folds):
    """
    Use cross validation to find the best K for the kNN model.

    :param ds_train: Training dataset.
    :param k_choices: A sequence of possible value of k for the kNN model.
    :param num_folds: Number of folds for cross-validation.
    :return: tuple (best_k, accuracies) where:
        best_k: the value of k with the highest mean accuracy across folds
        accuracies: The accuracies per fold for each k (list of lists).
    """

    accuracies = []
    for i, k in enumerate(k_choices):
        model = KNNClassifier(k)

        validation_ratio = 1/num_folds
        dl_train, dl_valid = dataloaders.create_train_validation_loaders(ds_train, validation_ratio)

        acc = []
        for fold in range(num_folds - 1):
            x_valid, y_valid = dataloader_utils.flatten(dl_valid)
            model.train(dl_train)
            prediction = model.predict(x_valid)
            acc.append(accuracy(
                y=y_valid,
                y_pred=prediction
            ))
        accuracies.append(acc)

    best_k_idx = np.argmax([np.mean(acc) for acc in accuracies])
    best_k = k_choices[best_k_idx]

    return best_k, accuracies
