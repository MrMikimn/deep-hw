import abc
import torch
import hw1.transforms as hw1tf


class ClassifierLoss(abc.ABC):
    """
    Represents a loss function of a classifier.
    """

    def __call__(self, *args, **kwargs):
        return self.loss(*args, **kwargs)

    @abc.abstractmethod
    def loss(self, *args, **kw):
        pass

    @abc.abstractmethod
    def grad(self):
        """
        :return: Gradient of the last calculated loss w.r.t. model
            parameters, as a Tensor of shape (D, C).
        """
        pass


class SVMHingeLoss(ClassifierLoss):
    def __init__(self, delta=1.0):
        self.delta = delta
        self.grad_ctx = {}

    def loss(self, x, y, x_scores, y_predicted):
        """
        Calculates the Hinge-loss for a batch of samples.

        :param x: Batch of samples in a Tensor of shape (N, D).
        :param y: Ground-truth labels for these samples: (N,)
        :param x_scores: The predicted class score for each sample: (N, C).
        :param y_predicted: The predicted class label for each sample: (N,).
        :return: The classification loss as a Tensor of shape (1,).
        """

        assert x_scores.shape[0] == y.shape[0]
        assert y.dim() == 1

        # TODO: Implement SVM loss calculation based on the hinge-loss formula.
        #  Notes:
        #  - Use only basic pytorch tensor operations, no external code.
        #  - Full credit will be given only for a fully vectorized
        #    implementation (zero explicit loops).
        #    Hint: Create a matrix M where M[i,j] is the margin-loss
        #    for sample i and class j (i.e. s_j - s_{y_i} + delta).

        # |x| ==> (N, D)
        # |x_scores| ==> (N, C)
        # |M| ==> (N, C). Each row is a sample marginal-loss
        j_rows = torch.arange(y.shape[0])
        m_mat = self.delta + x_scores - x_scores[j_rows, y].view(-1, 1)
        # if yi == j then M[yi, j] = 0
        m_mat[j_rows, y] = 0
        loss = torch.max(m_mat, torch.zeros_like(m_mat)).sum() / y.shape[0]

        # TODO: Save what you need for gradient calculation in self.grad_ctx
        self.grad_ctx['m_mat'] = m_mat
        self.grad_ctx['x'] = x
        self.grad_ctx['y'] = y

        return loss

    def grad(self):
        """
        Calculates the gradient of the Hinge-loss w.r.t. parameters.
        :return: The gradient, of shape (D, C).

        """
        # TODO:
        #  Implement SVM loss gradient calculation
        #  Same notes as above. Hint: Use the matrix M from above, based on
        #  it create a matrix G such that X^T * G is the gradient.

        m_mat = self.grad_ctx['m_mat']
        x = self.grad_ctx['x']
        y = self.grad_ctx['y']
        g_mat = torch.zeros_like(m_mat)
        g_mat[m_mat > 0] = 1
        g_mat[torch.arange(y.shape[0]), y] = -g_mat.sum(dim=1)
        # (D x N) x (N x C) ==> (D, C)
        grad = x.transpose(0, 1).mm(g_mat) / y.shape[0]

        return grad
