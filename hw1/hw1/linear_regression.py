import numpy as np
import sklearn
from sklearn.base import BaseEstimator, RegressorMixin, TransformerMixin
from sklearn.preprocessing import PolynomialFeatures
from pandas import DataFrame
from sklearn.utils import check_array
from sklearn.utils.validation import check_is_fitted, check_X_y
from sklearn.model_selection import KFold


class LinearRegressor(BaseEstimator, RegressorMixin):
    """
    Implements Linear Regression prediction and closed-form parameter fitting.
    """

    def __init__(self, reg_lambda=0.1):
        self.reg_lambda = reg_lambda

    def predict(self, X):
        """
        Predict the class of a batch of samples based on the current weights.
        :param X: A tensor of shape (N,n_features_) where N is the batch size.
        :return:
            y_pred: np.ndarray of shape (N,) where each entry is the predicted
                value of the corresponding sample.
        """
        X = check_array(X)
        check_is_fitted(self, 'weights_')

        # print(X.shape)
        # print(self.weights_.shape)
        # y_pred = np.sum(self.weights_.T * X, axis=1)
        y_pred = np.dot(X, self.weights_)
        return y_pred

    def fit(self, X, y):
        """
        Fit optimal weights to data using closed form solution.
        :param X: A tensor of shape (N,n_features_) where N is the batch size.
        :param y: A tensor of shape (N,) where N is the batch size.
        """
        X, y = check_X_y(X, y)

        I = np.identity(X.shape[1])
        n = len(X)
        I[0, 0] = 0
        X_XT_regularized = (1 / n) * X.T @ X + self.reg_lambda * I
        w_opt = np.linalg.inv(X_XT_regularized) @ ((1/n) * X.T) @ y
        self.weights_ = w_opt
        return self

    def fit_predict(self, X, y):
        return self.fit(X, y).predict(X)


class BiasTrickTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X: np.ndarray):
        """
        :param X: A tensor of shape (N,D) where N is the batch size and D is
        the number of features.
        :returns: A tensor xb of shape (N,D+1) where xb[:, 0] == 1
        """

        X = check_array(X, ensure_2d=True)
        xb = np.hstack((np.ones((X.shape[0], 1)), X))
        return xb


class BostonFeaturesTransformer(BaseEstimator, TransformerMixin):
    """
    Generates custom features for the Boston dataset.
    """

    def __init__(self, degree=2):
        self.degree = degree

        # Add any hyperparameters you need and save them as above

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        """
        Transform features to new features matrix.
        :param X: Matrix of shape (n_samples, n_features_).
        :returns: Matrix of shape (n_samples, n_output_features_).
        """
        X = check_array(X)

        poly = PolynomialFeatures(self.degree, include_bias=False)
        X_new = np.delete(X[:, 1:], 1, axis=1)
        X_transformed = poly.fit_transform(X_new)
        return X_transformed


def top_correlated_features(df: DataFrame, target_feature, n=5):
    """
    Returns the names of features most strongly correlated (correlation is
    close to 1 or -1) with a target feature. Correlation is Pearson's-r sense.

    :param df: A pandas dataframe.
    :param target_feature: The name of the target feature.
    :param n: Number of top features to return.
    :return: A tuple of
        - top_n_features: Sequence of the top feature names
        - top_n_corr: Sequence of correlation coefficients of above features
        Both the returned sequences should be sorted so that the best (most
        correlated) feature is first.
    """

    series = df.corrwith(other=df[target_feature]).drop(target_feature).abs().nlargest(n)
    top_n_features = list(series.index)
    top_n_corr = list(series.values)

    return top_n_features, top_n_corr


def mse_score(y: np.ndarray, y_pred: np.ndarray):
    """
    Computes Mean Squared Error.
    :param y: Predictions, shape (N,)
    :param y_pred: Ground truth labels, shape (N,)
    :return: MSE score.
    """

    mse = (np.square(y - y_pred)).mean()
    return mse


def r2_score(y: np.ndarray, y_pred: np.ndarray):
    """
    Computes R^2 score,
    :param y: Predictions, shape (N,)
    :param y_pred: Ground truth labels, shape (N,)
    :return: R^2 score.
    """

    y_mean = y.mean()
    sum_1 = (np.square(y - y_pred)).sum()
    sum_2 = (np.square(y - y_mean)).sum()
    r2 = 1 - sum_1 / sum_2
    return r2


def cv_best_hyperparams(model: BaseEstimator, X, y, k_folds,
                        degree_range, lambda_range):
    """
    Cross-validate to find best hyperparameters with k-fold CV.
    :param X: Training data.
    :param y: Training targets.
    :param model: sklearn model.
    :param lambda_range: Range of values for the regularization hyperparam.
    :param degree_range: Range of values for the degree hyperparam.
    :param k_folds: Number of folds for splitting the training data into.
    :return: A dict containing the best model parameters,
        with some of the keys as returned by model.get_params()
    """

    min_accuracy = np.inf
    curr_deg = None
    curr_lamb = None
    kf = KFold(n_splits=k_folds)
    for deg in degree_range:
        for lamb in lambda_range:
            new_dict = model.get_params()
            new_dict['bostonfeaturestransformer__degree'] = deg
            new_dict['linearregressor__reg_lambda'] = lamb
            model.set_params(**new_dict)
            for train_index, test_index in kf.split(X):
                train_data_X, train_data_y, test_data_X, test_data_y = X[train_index], y[train_index], \
                                                                       X[test_index], y[test_index]
                model.fit(train_data_X, train_data_y)
                y_pred = model.predict(test_data_X)
                accuracy = mse_score(test_data_y, y_pred)
                if accuracy < min_accuracy:
                    min_accuracy = accuracy
                    curr_deg = deg
                    curr_lamb = lamb

    best_params = model.get_params()
    best_params['bostonfeaturestransformer__degree'] = curr_deg
    best_params['linearregressor__reg_lambda'] = curr_lamb
    return best_params
