import torch
from torch import Tensor
from torch.utils.data import DataLoader
from collections import namedtuple
import hw1.transforms as hw1tf
import numpy as np

from .losses import ClassifierLoss


class LinearClassifier(object):

    def __init__(self, n_features, n_classes, weight_std=0.001):
        """
        Initializes the linear classifier.
        :param n_features: Number or features in each sample.
        :param n_classes: Number of classes samples can belong to.
        :param weight_std: Standard deviation of initial weights.
        """
        self.n_features = n_features
        self.n_classes = n_classes

        # TODO:
        #  Create weights tensor of appropriate dimensions
        #  Initialize it from a normal dist with zero mean and the given std.

        self.norm = torch.distributions.Normal(loc=0, scale=weight_std)
        self.weights = self.norm.sample((n_features, n_classes))

    def predict(self, x: Tensor):
        """
        Predict the class of a batch of samples based on the current weights.
        :param x: A tensor of shape (N,n_features) where N is the batch size.
        :return:
            y_pred: Tensor of shape (N,) where each entry is the predicted
                class of the corresponding sample. Predictions are integers in
                range [0, n_classes-1].
            class_scores: Tensor of shape (N,n_classes) with the class score
                per sample.
        """

        # TODO:
        #  Implement linear prediction.
        #  Calculate the score for each class using the weights and
        #  return the class y_pred with the highest score.

        # bias_trick = hw1tf.BiasTrick()
        # x_biased = bias_trick(x)

        # (N, D + 1) x (D + 1, C) ==> (N, C)
        class_scores = x.mm(self.weights)
        y_pred = class_scores.argmax(1)
        # y_pred = class_scores[torch.argmax(class_scores)]
        return y_pred, class_scores

    @staticmethod
    def evaluate_accuracy(y: Tensor, y_pred: Tensor):
        """
        Calculates the prediction accuracy based on predicted and ground-truth
        labels.
        :param y: A tensor of shape (N,) containing ground truth class labels.
        :param y_pred: A tensor of shape (N,) containing predicted labels.
        :return: The accuracy in percent.
        """

        # TODO:
        #  calculate accuracy of prediction.
        #  Use the predict function above and compare the predicted class
        #  labels to the ground truth labels to obtain the accuracy (in %).
        #  Do not use an explicit loop.
        res = np.array(y == y_pred).sum()
        acc = (res / y.size())[0]
        return acc * 100

    def train(self,
              dl_train: DataLoader,
              dl_valid: DataLoader,
              loss_fn: ClassifierLoss,
              learn_rate=0.1, weight_decay=0.001, max_epochs=100):

        Result = namedtuple('Result', 'accuracy loss')
        train_res = Result(accuracy=[], loss=[])
        valid_res = Result(accuracy=[], loss=[])

        # TODO:
        #  Implement model training loop.
        #  1. At each epoch, evaluate the model on the entire training set
        #     (batch by batch) and update the weights.
        #  2. Each epoch, also evaluate on the validation set.
        #  3. Accumulate average loss and total accuracy for both sets.
        #     The train/valid_res variables should hold the average loss
        #     and accuracy per epoch.
        #  4. Don't forget to add a regularization term to the loss,
        #     using the weight_decay parameter.

        print('Training', end='')
        for epoch_idx in range(max_epochs):
            total_correct_train = 0
            average_loss_train = 0
            n = 0

            for idx, (x, y) in enumerate(dl_train):
                n += x.shape[0]
                y_predicted, x_scores = self.predict(x)

                regularized_loss = loss_fn(x, y, x_scores, y_predicted) + (weight_decay / 2) *\
                                   (self.weights.norm() ** 2)
                average_loss_train += regularized_loss

                grad = loss_fn.grad() + (weight_decay * self.weights)
                self.weights = self.weights - learn_rate * grad

                acc_train = self.evaluate_accuracy(y, y_predicted)
                total_correct_train += acc_train

            train_res.accuracy.append(total_correct_train / n)
            train_res.loss.append(average_loss_train / n)

            total_correct_valid = 0
            average_loss_valid = 0
            n = 0

            for idx, (x, y) in enumerate(dl_valid):
                n += x.shape[0]
                y_predicted, x_scores = self.predict(x)

                regularized_loss = loss_fn(x, y, x_scores, y_predicted) + (weight_decay / 2) * \
                                   (self.weights.norm() ** 2)
                average_loss_valid += regularized_loss

                acc_train = self.evaluate_accuracy(y, y_predicted)
                total_correct_valid += acc_train

            valid_res.accuracy.append(total_correct_valid / n)
            valid_res.loss.append(average_loss_valid / n)

            print('.', end='')

        print('')
        return train_res, valid_res

    def weights_as_images(self, img_shape, has_bias=True):
        """
        Create tensor images from the weights, for visualization.
        :param img_shape: Shape of each tensor image to create, i.e. (C,H,W).
        :param has_bias: Whether the weights include a bias component
            (assumed to be the first feature).
        :return: Tensor of shape (n_classes, C, H, W).
        """

        weights = self.weights
        (C, H, W) = img_shape
        if has_bias:
            weights = weights[:-1]
        w_images = weights.transpose(0, 1).reshape((self.n_classes, C, H, W))
        return w_images


def hyperparams():
    # TODO:
    #  Manually tune the hyperparameters to get the training accuracy test
    #  to pass.
    hp = dict(weight_std=0.05, learn_rate=0.05, weight_decay=0.02)
    return hp
