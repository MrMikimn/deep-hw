r"""
Use this module to write your answers to the questions in the notebook.

Note: Inside the answer strings you can use Markdown format and also LaTeX
math (delimited with $$).
"""

# ==============
# Part 2 answers


def part2_overfit_hp():
    wstd, lr, reg = 0.1, 0.1, 0.01
    # TODO: Tweak the hyperparameters until you overfit the small dataset.
    return dict(wstd=wstd, lr=lr, reg=reg)


def part2_optim_hp():
    # wstd, lr_vanilla, lr_momentum, lr_rmsprop, reg, = 0.05, 0.02, 0.006, 0.01, 0.005
    wstd, lr_vanilla, lr_momentum, lr_rmsprop, reg, = 1, 0.02, 0.01, 0.00014, 0.006
    # TODO: Tweak the hyperparameters to get the best results you can.
    # You may want to use different learning rates for each optimizer.
    return dict(wstd=wstd, lr_vanilla=lr_vanilla, lr_momentum=lr_momentum,
                lr_rmsprop=lr_rmsprop, reg=reg)


def part2_dropout_hp():
    wstd, lr, = 0.08, 0.0002
    # TODO: Tweak the hyperparameters to get the model to overfit without
    # dropout.
    return dict(wstd=wstd, lr=lr)


part2_q1 = r"""
**Answer:**

1. We can see that for the no-dropout graph, we indeed achieve overfitting by tuning the
hyperparams, and specifically decreasing the learning rate by a factor. 
This means that the weights and biases of the model are highly fitted to the training
data, which leads to significantly low accuracy for the test data. We've expected to see that
as we increase the dropout probability $p$, the overfitting should be less significant, and
both the training and test phases will start to converge to a similar accuracy. We can see
this phenomenon occur in the graphs (and specifically with dropout = 0.8) and can attribute it
to the fact that essentially, when we apply a dropout $p$ to a fully-connected layer, on average
we dismiss $p$ of our activations. That means, for example, that for $p = 0.8$, we will look at
only $20%$ of our neurons in each layer on average, and that decreases the chance to overfit.

2. As we said, we've expected that when the dropout increases, we decrease the chance to overfit.
Furthermore, we see that for low dropout we get better test results than both the no-dropout graph
and the high dropout graph. This can be explained in 2 ways: The no-dropout model is highly overfitted,
so it makes sense that if we remove a fraction of the neurons, we expect to see slightly better results
for the test data. But on top of that, if our dropout were too high, we could remove too many neurons for
the model to actually do significant learning, since now we lose too much of our model parameters,
and the needs a lot more epochs to get the desired accuracy and generalize from the data. This means
that for some $0 < p < 1$ we expect to see the best results (on average), and so if we were to run this model
with various more dropout probabilities and plot their accuracies against $p$, we would probably see a maximum
somewhere in the middle

"""

part2_q2 = r"""
**Your answer:**

When training a model with the cross-entropy loss function,
it is possible for the test loss to increase for a few epochs while the test accuracy also increases.
First, in simple words, accuracy measures whether you get the prediction right 
(and how much correct labels you got in total), but cross-entropy measures how confident you are
about a prediction. The accuracy distinguishes only between the predication to the true 
classification (0 or 1),  but the CE in the widest range (0 to 1). In addition, some outliers 
can affect a lot in CE,  in case they are not close at all to the true classification,
by increasing the loss function significantly, with minimal effect to the accuracy 
(since there we look only the difference between the labels - 0 or 1). Therefore, 
this is a possible scenario that the test loss to increase for a few epochs while the
test accuracy also increases.

"""
# ==============

# ==============
# Part 3 answers

part3_q1 = r"""
**Answer:**

In experiment 1.1 we can see from the graphs, that as the network deepens, the accuracy decreases. 
The deeper the network is, we are able to extract more features, and more complex patterns, but when
the model is too deep we will start seeing the "Vanishing gradients" problem, as described below.
We got the best accuracy with $L=4$ (and $K=32$). When $L$ increases, the model 
is not trained at all, as we can see with $L=8,16$. In addition, with $L=2$, we are 
getting good results, but with a small decrease in accuracy compared to $L=4$. 
Furthermore, $L=4$ depth produces the best results since adding layers to the CNN increases 
the number of weights in the network, ergo the model complexity. Without a large training
set (like in our situation), an increasingly large network is likely to overfit and in turn, 
reduce accuracy on the test data. We see that our model is overfitting since we get a difference between
the training and testing accuracies.
We suffer from what is known as the Vanishing gradient problem, where
the model complexity influences how our model is unable to train from small datasets.

As described above, if we can use more data to train out model (increasing training set), 
the problem that we got (that the model is too big) will improve, with some tradeoff 
as well of overfitting that can occur. Moreover, we can use more filters (increasing K). 
By this, we will handle complex features in a better way,  even duo L (depth) is big.
Another option will be to adding Residual / skip connections (to reduce the length of flow and allow
gradients to flow freely).

"""

part3_q2 = r"""
**Answer:**

We notice that for a constant $L=2$, increasing the number of filters in the convolutional layers also
increases the accuracy of the model and reduces the loss. Consequently, $K=256$ yields the best results.
In this case, we have a pretty shallow network with only 2 repetitions. This means that for lower values
of $K$, the number of filters is not enough to train the model and we don't have a big enough convolution
to be able to extract significant features from the images, since  we have less filters to work with.
for a constant $L=4$, we see the same pattern emerge, as increasing the number of filters also generally
increases the train/test accuracy. 

as for  $L=8$, this is where things start to blow up. A network this deep is likely to experience the
vanishing gradient effect, where too many parameters lead to the network not being able to learn from
examples at all. That's likely the reason we are getting poor results in contrast to smaller values of $L$.
For values of $K=128,256$ we are still getting good results, since we maintain the ratio between $K$ and $L$
and so the case is similar to smaller networks.

For smaller models (due to the increase in $K$) we are more likely to overfit, because we are using more
filters, and thus extracting more information from our training set early on.

We understand from 1.1 and 1.2 that in order to increase the accuracy of the model in general, we need to
maintain a **ratio** between $K$ and $L$ such that the network is neither to complex to learn from our 
dataset (vanishing gradients), nor too simple to extract meaningful features. 

"""

part3_q3 = r"""
**Answer:**

For this experiment, the best result we got was with $L=2$. With $L=1$ we got similarly good results, but the
model was a bit too overfitted (similarly to 1.2, where more filters meant more overfitting)
As we've seen from the previous experiments, increasing $L$ generally led to a decrease in accuracy, since the
network was to complex to do meaningful learning. We expect that as we add more filters to the layer, this
effect will be even stronger, whereas the very addition of the various filters should lead to better results
since the network is able to extract more diverse and meaningful features. This is indeed what we experience
in the graphs, where for small $L$ values we get better accuracy than previous tests as $L$ increases, and 
as we increase $L$ the overall train/test accuracy decreases up to a point where the ratio is too big. 
For big enough $L$'s we see that the network is unable to learn at all, since it now as 8 and 12 layer-deep 
feature extractors, which correlates to what we've seen in 1.1, 1.2

"""

part3_q4 = r"""
**Answer:**

In the first part of experiment 1.4, we got that for $L=8,16$, we are getting better 
results compared to those in experiment 1.1, since in this experiment we increased K. 
When $L=32$, the increase in $L$ is not enough and the model's train accuracy decreases
(the same problem of the model getting bigger and more complex). We see that there is less overfitting
for each of the parameters, since we are dealing with much deeper networks.

By using residual blocks we are able to increase L, while improving the accuracy for
those network sizes compared to standard CNN implementations. 
The residual blocks solve much of the vanishing gradients problem that we had, 
since by using the skip connection the model is able to train much better with bigger values of $L$.
In the second part of the experiment, we can see that now for larger $L$ sizes than those 
in experiment 1.3, we are getting better performances. This is because the same reason as in part 1.

"""

part3_q5 = r"""
**Answer:**

1. For this experiment we've used the same basic outline for ResNet with 2 main modifications: The addition
of BatchNorm layers and a dropout of 0.25. BatchNorm layers generally lead to faster training, and improve
accuracy of the model, as they make sure that the mean of received input is 0 and the variance is 1. This 
leads to better regularization and generally better training. As for dropout, since we scale networks both
by the number of layers, and the number of filters in each layer, we start seeing models too complex to
train our data on. So adding a small dropout means that we reduce the number of parameters in the model,
making it more robust to changes in network depth and number of filters.

2. We still see a pattern, where as we increase the number of layers, the overall accuracy decreases. But we
notice that now we are able to train much deeper networks with less overfitting and with a bit more accuracy,
since we use Residual blocks
to ensure that shortcuts allow gradients to flow. and dropout + batchnorm to increase the capacity of the network
for layers and improve the accuracy. 

"""
# ==============
