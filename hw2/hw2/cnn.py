import torch
import itertools as it
import torch.nn as nn


class ConvClassifier(nn.Module):
    """
    A convolutional classifier model based on PyTorch nn.Modules.

    The architecture is:
    [(CONV -> ReLU)*P -> MaxPool]*(N/P) -> (Linear -> ReLU)*M -> Linear
    """

    def __init__(self, in_size, out_classes: int, channels: list,
                 pool_every: int, hidden_dims: list):
        """
        :param in_size: Size of input images, e.g. (C,H,W).
        :param out_classes: Number of classes to output in the final layer.
        :param channels: A list of of length N containing the number of
            (output) channels in each conv layer.
        :param pool_every: P, the number of conv layers before each max-pool.
        :param hidden_dims: List of of length M containing hidden dimensions of
            each Linear layer (not including the output layer).
        """
        super().__init__()
        assert channels and hidden_dims

        self.in_size = in_size
        self.out_classes = out_classes
        self.channels = channels
        self.pool_every = pool_every
        self.num_pools = 0
        self.cur_in_size = in_size
        self.hidden_dims = hidden_dims

        self.feature_extractor = self._make_feature_extractor()
        self.classifier = self._make_classifier()

    def _make_feature_extractor(self):
        in_channels, in_h, in_w, = tuple(self.in_size)

        layers = []
        # TODO: Create the feature extractor part of the model:
        #  [(CONV -> ReLU)*P -> MaxPool]*(N/P)
        #  Use only dimension-preserving 3x3 convolutions. Apply 2x2 Max
        #  Pooling to reduce dimensions after every P convolutions.
        #  Note: If N is not divisible by P, then N mod P additional
        #  CONV->ReLUs should exist at the end, without a MaxPool after them.

        P = self.pool_every
        self.cur_in_size = self.in_size
        cur_in_channels, cur_in_h, cur_in_w, = tuple(self.cur_in_size)

        for cur_channel in self.channels:
            layers.append(nn.Conv2d(in_channels=cur_in_channels,
                                    out_channels=cur_channel,
                                    kernel_size=3,
                                    padding=1))
            layers.append(nn.ReLU())
            cur_in_channels = cur_channel
            self.num_pools += 1

            if self.num_pools == P:
                layers.append(nn.MaxPool2d(2))
                self.num_pools = 0
                cur_in_h, cur_in_w = int(cur_in_h / 2), int(cur_in_w / 2)
                self.cur_in_size = cur_in_channels, cur_in_h, cur_in_w
            else:
                self.cur_in_size = cur_in_channels, self.cur_in_size[1], self.cur_in_size[2]

        seq = nn.Sequential(*layers)
        return seq

    def _make_classifier(self):
        in_channels, in_h, in_w, = tuple(self.in_size)
        layers = []
        # TODO: Create the classifier part of the model:
        #  (Linear -> ReLU)*M -> Linear
        #  You'll first need to calculate the number of features going in to
        #  the first linear layer.
        #  The last Linear layer should have an output dim of out_classes.

        cur_in_channels, cur_in_h, cur_in_w, = tuple(self.cur_in_size)
        current_in_features = cur_in_channels * cur_in_h * cur_in_w
        M = len(self.hidden_dims)

        for i in range(M):
            out_features = self.hidden_dims[i]
            layers.append(nn.Linear(in_features=current_in_features, out_features=out_features))
            layers.append(nn.ReLU())
            current_in_features = out_features

        layers.append(nn.Linear(in_features=current_in_features, out_features=self.out_classes))

        seq = nn.Sequential(*layers)
        return seq

    def forward(self, x):
        # TODO: Implement the forward pass.
        #  Extract features from the input, run the classifier on them and
        #  return class scores.
        features = self.feature_extractor(x)
        features = features.view(features.size(0), -1)
        out = self.classifier(features)
        return out


class ResidualBlock(nn.Module):
    """
    A general purpose residual block.
    """

    def __init__(self, in_channels: int, channels: list, kernel_sizes: list,
                 batchnorm=False, dropout=0.):
        """
        :param in_channels: Number of input channels to the first convolution.
        :param channels: List of number of output channels for each
        convolution in the block. The length determines the number of
        convolutions.
        :param kernel_sizes: List of kernel sizes (spatial). Length should
        be the same as channels. Values should be odd numbers.
        :param batchnorm: True/False whether to apply BatchNorm between
        convolutions.
        :param dropout: Amount (p) of Dropout to apply between convolutions.
        Zero means don't apply dropout.
        """
        super().__init__()
        assert channels and kernel_sizes
        assert len(channels) == len(kernel_sizes)
        assert all(map(lambda x: x % 2 == 1, kernel_sizes))

        self.main_path, self.shortcut_path = None, None

        # TODO: Implement a generic residual block.
        #  Use the given arguments to create two nn.Sequentials:
        #  - main_path, which should contain the convolution, dropout,
        #    batchnorm, relu sequences (in this order). Should end with a
        #    final conv as in the diagram.
        #  - shortcut_path which should represent the skip-connection and
        #    may contain a 1x1 conv.
        #  Notes:
        #  - Use convolutions which preserve the spatial extent of the input.
        #  - Use bias in the main_path conv layers, and no bias in the skips.
        #  - For simplicity of implementation, assume kernel sizes are odd.
        #  - Don't create layers which you don't use. This will prevent
        #    correct comparison in the test.
        layers = []
        shortcut = []
        current_in_channels = in_channels
        num_blocks = len(channels)
        for out_channels, kernel_size in zip(channels, kernel_sizes):
            padding = (kernel_size - 1) // 2
            layers.append(nn.Conv2d(current_in_channels, out_channels, kernel_size, padding=padding, bias=True))
            if num_blocks > 1:
                current_in_channels = out_channels
                if dropout > 0:
                    layers.append(nn.Dropout2d(p=dropout))
                if batchnorm:
                    layers.append(nn.BatchNorm2d(current_in_channels))
                layers.append(nn.ReLU())
            num_blocks -= 1

        self.main_path = nn.Sequential(*layers)
        if in_channels != channels[-1]:
            shortcut.append(nn.Conv2d(in_channels, channels[-1], kernel_size=(1, 1), bias=False))
        self.shortcut_path = nn.Sequential(*shortcut)

    def forward(self, x):
        out = self.main_path(x)
        out += self.shortcut_path(x)
        out = torch.relu(out)
        return out


class ResNetClassifier(ConvClassifier):
    def __init__(self, in_size, out_classes, channels, pool_every,
                 hidden_dims):
        super().__init__(in_size, out_classes, channels, pool_every,
                         hidden_dims)

    def _make_feature_extractor(self):
        in_channels, in_h, in_w, = tuple(self.in_size)

        layers = []
        P = self.pool_every
        N = len(self.channels)
        self.cur_in_size = self.in_size
        cur_in_channels, cur_in_h, cur_in_w, = tuple(self.cur_in_size)

        for cur_channel_i in range(0, len(self.channels), P):

            block_channels = self.channels[cur_channel_i:cur_channel_i + P]
            layers.append(ResidualBlock(cur_in_channels, block_channels, kernel_sizes=([3] * len(block_channels))))
            cur_in_channels = block_channels[-1]
            if cur_channel_i + P <= N:
                layers.append(nn.MaxPool2d(2))
                cur_in_h, cur_in_w = int(cur_in_h / 2), int(cur_in_w / 2)
                self.cur_in_size = cur_in_channels, cur_in_h, cur_in_w
            else:
                self.cur_in_size = cur_in_channels, self.cur_in_size[1], self.cur_in_size[2]

        seq = nn.Sequential(*layers)
        return seq


class YourCodeNet(ConvClassifier):
    def __init__(self, in_size, out_classes, channels, pool_every,
                 hidden_dims):
        super().__init__(in_size, out_classes, channels, pool_every,
                         hidden_dims)

    # TODO: Change whatever you want about the ConvClassifier to try to
    #  improve it's results on CIFAR-10.
    #  For example, add batchnorm, dropout, skip connections, change conv
    #  filter sizes etc.
    def _make_feature_extractor(self):
        layers = []
        P = self.pool_every
        N = len(self.channels)
        cur_in_channels, cur_in_h, cur_in_w, = tuple(self.cur_in_size)

        for cur_channel_i in range(0, len(self.channels), P):

            block_channels = self.channels[cur_channel_i:cur_channel_i + P]
            layers.append(ResidualBlock(
                cur_in_channels,
                block_channels,
                kernel_sizes=([3] * len(block_channels)),
                batchnorm=True,
                dropout=0.25
            ))
            cur_in_channels = block_channels[-1]
            if cur_channel_i + P <= N:
                layers.append(nn.MaxPool2d(2))
                cur_in_h, cur_in_w = int(cur_in_h / 2), int(cur_in_w / 2)
                self.cur_in_size = cur_in_channels, cur_in_h, cur_in_w
            else:
                self.cur_in_size = cur_in_channels, self.cur_in_size[1], self.cur_in_size[2]

        seq = nn.Sequential(*layers)
        return seq
