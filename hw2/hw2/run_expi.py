
if __name__ == '__main__':
    import os
    print("[Experiment 1.1]")
    os.system("for l in 16 8 4 2; do for k in 32 64; "
              "do srun -c 2 -w rishon4 --gres=gpu:1 --pty python -m hw2.experiments run-exp "
              "-n exp1_1 -K $k -L $l -M cnn "
              "-P 5 -H 100; "
              "done; "
              "done")
    print("[Experiment 1.1] Done.")

    print("[Experiment 1.2]")
    os.system("for l in 8 4 2; "
              "do for k in 32 64 128 256; do srun -c 2 -w rishon4 --gres=gpu:1 --pty python -m hw2.experiments run-exp "
              "-n exp1_2 -K $k -L $l -M cnn -P 3 -H 100 --lr 0.0002; "
              "done; "
              "done")
    print("[Experiment 1.2] Done")

    print("[Experiment 1.3]")
    os.system("for l in 4 3 2 1; do srun -c 2 -w rishon4 --gres=gpu:1 --pty python -m hw2.experiments run-exp "
              "-n exp1_3 -K 64 128 256 -L $l -M cnn -P 3 -H 100; "
              "done")
    print("[Experiment 1.3] Dome")

    print("[Experiment 1.4] K=32")
    os.system("for l in 32 16 8; do srun -c 2 -w rishon4 --gres=gpu:1 --pty python -m hw2.experiments run-exp "
              "-n exp1_4 -K 32 -L $l -M resnet -P 5 -H 100; "
              "done")
    print("[Experiment 1.4] Done")

    print("[Experiment 1.4] K=[64 128 256]")
    os.system("for l in 8 4 2; do srun -c 2 -w rishon4 --gres=gpu:1 --pty python -m hw2.experiments run-exp "
              "-n exp1_4 -K 64 128 256 -L $l -M resnet -P 5 -H 100; "
              "done")
    print("[Experiment 1.4] Done")

    print("[Experiment 2]")
    os.system("for l in 12 9 6 3; "
              "do srun -c 2 -w rishon4 --gres=gpu:1 --pty python -m hw2.experiments run-exp "
              "-n exp2 -K 32 64 128 -L $l -M ycn -P 8 -H 100; --lr 0.01; "
              "done")
    print("[Experiment 2] Done")