r"""
Use this module to write your answers to the questions in the notebook.

Note: Inside the answer strings you can use Markdown format and also LaTeX
math (delimited with $$).
"""

# ==============
# Part 1 answers


def part1_rnn_hyperparams():
    hypers = dict(
        batch_size=256, seq_len=64,
        h_dim=256, n_layers=3, dropout=0.25,
        learn_rate=0.04, lr_sched_factor=0.1, lr_sched_patience=1,
    )
    # TODO: Set the hyperparameters to train the model.
    return hypers


def part1_generation_params():
    start_seq = "ACT I. SCENE 1. Rousillon. The COUNT'S palace\n Enter BERTRAM"
    temperature = 0.75
    # TODO: Tweak the parameters to generate a literary masterpiece.
    return start_seq, temperature


part1_q1 = r"""
**Answer:**

We are training our model on sequences instead of the entire corpus, mainly because of size limitations. Theoretically,
we could generate labels for the entire corpus at once and train on that, but it is simply too big too fit inside the
memory of a GPU, and would take too much time and memory to train on. Furthermore, if we train our model on big chunks
of text each time (for example, the entire corpus), we might experience a case of vanishing or exploding gradients,
where we are unable to backpropagate the model due to the complexity of the model/data.

"""

part1_q2 = r"""
**Answer:**

First, let's notice how we sample batches from the data: Given batches $B_1^{N\times V}, B_2^{N\times V}$, we consider
that the samples $B_1[i]$ and $B_2[i]$ correspond to trailing sequences of text from the corpus. We also notice that
for a given epoch, we feed $h_n$ of batch $j$ as $h_0$ of batch $j + 1$. This acts as a link between the trailing 
sequences in each batch, allowing the batch to "remember" what the epoch has learned up to the given point in text.
We can think of an epoch as chaining smaller sequences of text together, creating a much larger memory per-epoch.
Between epochs we zero-out our hidden state, "wiping" the memory clean

"""

part1_q3 = r"""
**Answer:**

Following the explanation of question 2, we know that the batches are sampled such that there is a correlation between
batch $i$ and batch $i + 1$ (they contain leading text sequences). This, shuffling the batches will result in losing
the order between the batches, and thus the learning process will not be able to maintain the long memory chains, as a
result we will lose the context of the text and might generate gibberish or even meaningless words.

"""

part1_q4 = r"""
**Answer:**

1. We lower the temperature for sampling, to achieve bigger margins between our probabilities, as generated using the
Softmax function on our output predictions. This is done to make sure that characters with very similar probabilities
are distinguishable by the model, and thus the model is more confident when selection the next character.
Since we have a big vocabulary, and consequently a big out-dimension, our variance will tend to be low, meaning that
there as a bigger chance that the probabilities will be sensitive to small changes. Lowering the temperature balances
this effect by increasing the variance of the output probability space.

2. The variance will be lower and the probability space of the output will tend to be more uniform, meaning that small
changes in the model can produce very similar probabilities and lead to wrong predictions, and random-looking text.
Performing softmax on larger values makes the RNN more confident (less input is needed to activate the output layer) 
but also more conservative in its samples (it is less likely to sample from unlikely candidates). 

3. Using a low temperature produces a less-uniform probability distribution over the classes, and makes the RNN less 
"excited" by samples, resulting in less diversity and more repetitions in the text.

"""
# ==============


# ==============
# Part 2 answers

PART2_CUSTOM_DATA_URL = None


def part2_vae_hyperparams():
    hypers = dict(
        batch_size=64,
        h_dim=128, z_dim=32, x_sigma2=0.0001,
        learn_rate=0.0003, betas=(0.8, 0.999),
    )
    # TODO: Tweak the hyperparameters to generate a former president.
    return hypers


part2_q1 = r"""
**Answer:**

The $\sigma^2$ hyper-parameter is used to determine the weight of the data loss w.r.t the KL-divergence loss. higher
values of $\sigma^2$ give more significance to the data loss, while lower values give less significance. This means that
for lower values, our representation of the latent space will be less restricted and will produce images which look
different, but less like real images. For higher values, our representation of the latent space will be more dependant
of the data we are seeing and we will see images that look more like real images, but with less diversity.

"""

part2_q2 = r"""
**Answer:**
1. First, the reconstruction loss (data loss) is part of the VAE loss in purpose to generate images that are close 
(similar) to the data set — this is our regression term which tries to fit the model to the given dataset. 
Secondly, the KL divergence between two probability distributions simply measures how much 
they diverge from each other. Intuitively, this loss encourages the encoder to distribute all encodings, evenly around 
the center of the latent space (more specifically: it tries to learn a representation of a latent space that is normally
distributed, meaning that that latent point will be evenly spaced around the mean with some variance).
If it tries to "cheat" by clustering them apart into specific regions, away from the origin, it will be penalized.
In practice, adding the KL loss term as regularization helps us create a more "generative" model, which benefits from
being able to generate new data, given random points in the latent space.

2. As we've said in (1), we treat the KL-divergence as a regularization term of the loss function, as
we try and minimize the distance of the latent space posterior from a normal distribution. This means that as we give
more weight to the KL loss term, we will get a latent space where the points are evenly spaced around the center, and
thus when we use is to generate decoded data from the random points in the latent space, we will get data which does
not vary (i.e images which look almost the same) but they will no longer resemble our training data, since we force
them into a latent space which causes too much information loss.

3. The benefit of this is as we said, to give significant impact to the KL part, and as a result
to get the input's distribution as closest as the latent space's distribution, and a more generative model as well.
We assume that given enough samples, they fall close enough withing a normal distribution (CLT) such that our mapping
will be good at mapping randomly sampled latent space points to generated instances (images).
"""

# ==============

# ==============
# Part 3 answers

PART3_CUSTOM_DATA_URL = None


def part3_gan_hyperparams():
    hypers = dict(
        batch_size=64, z_dim=32,
        data_label=1, label_noise=0.25,
        discriminator_optimizer=dict(
            type='Adam',  # Any name in nn.optim like SGD, Adam
            lr=0.00017,
            # You an add extra args for the optimizer here
            betas=(0.5, 0.999),
            weight_decay=0.02
        ),
        generator_optimizer=dict(
            type='Adam',  # Any name in nn.optim like SGD, Adam
            lr=0.00017,
            # You an add extra args for the optimizer here
            betas=(0.5, 0.999),
            weight_decay=0.02
        ),
    )
    # TODO: Tweak the hyperparameters to train your GAN.
    return hypers


part3_q1 = r"""
**Answer:**

During training we sometimes need to maintain gradients when sampling from the GAN, and other times we don't, since
when we training the discriminator, we need to avoid forward-passing of the parameters, and to detach the generated 
samples. We want of course to train both of the discriminator and generator with the same samples, as they should 
work together by the model description. By this, we have to generate the samples only once at the beginning,
with `grad=True`, and then on the discriminator training to `detach` the parameters to avoiding forward-passing of the 
parameters. Therefore, we should discard the GAN samples when updating (training) the discriminator, and then maintain 
them when training the generator. 

"""

part3_q2 = r"""
**Your answer:**

1) When training a GAN to generate images, we should not decide to stop training solely based on the fact that the
Generator loss is below some threshold. As we saw in the exercise the Generator and the Discriminator have different
loss functions (generator and discriminator are competing against each other, hence improvement on the one means the
higher loss on the other, until this other learns better on the received loss, which screws up its competitor).
Therefore, we must decide to stop training by both of the loss functions to get a good result - creating real samples
and distinguish between them in a good form. 

2) If the discriminator loss remains at a constant value while the generator loss decreases, it means that the 
generator is much "weaker" than the discriminator, so it benefits from the decrease in loss (meaning, it learns to
generate more realistic images) while the discriminator stays at a threshold where he is still better than the generator
and therefore maintains his confidence in identifying the fake images.
"""

part3_q3 = r"""
**Answer:**

It seems that our VAE model is not particularly good when generating images with it since the images are a bit
blurry. The VAE loss function looks for minimizing the error in each pixel individually. Therefore, we are getting 
images which resemble the original images, but all the pixels are within some error rate of the original image,
resulting in an image which looks blurred.
In the opposite, GANs learn a loss function rather than using an existing one and they learn a loss that 
tries to classify if the output image is real or fake, while simultaneously training a generative model to minimize
this loss. The GAN produces "crisper" images is a result of not using an artificial, user-defined loss function 
(usually L1 or L2) but a loss that is learned from the data with the discriminator, and this it gives the generator
more power to learn significant features of the image (facial, clothing, background) rather than globally trying to
approximate pixel values. By this, it seems that the GAN performs better in this task.

"""

# ==============


