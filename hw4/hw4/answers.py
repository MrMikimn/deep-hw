r"""
Use this module to write your answers to the questions in the notebook.

Note: Inside the answer strings you can use Markdown format and also LaTeX
math (delimited with $$).
"""


# ==============
# Part 1 answers

def part1_pg_hyperparams():
    hp = dict(batch_size=12,
              gamma=0.98,
              beta=0.5,
              learn_rate=3e-3,
              eps=1e-8,
              )
    # TODO: Tweak the hyperparameters if needed.
    #  You can also add new ones if you need them for your model's __init__.
    return hp


def part1_aac_hyperparams():
    hp = dict(batch_size=16,
              gamma=0.96,
              beta=0.93,
              delta=0.93,
              learn_rate=0.0012,
              eps=1e-8,
              )
    # TODO: Tweak the hyperparameters. You can also add new ones if you need
    #   them for your model implementation.
    return hp


part1_q1 = r"""
**Your answer:**
When we subtract a baseline in the policy-gradient it helps to reduce its variance. 
As we know in policy gradient methods, we update the policy in the direction of received reward.
In addition, in a complicated task, the policy may receive very different rewards for similar behaviors.
We want our policy to be robust to the absolute reward function's value, and to differentiate between actions based
on their relative performance to the alternative actions. The average reward is the baseline. 
The advantage tries to quantify how much the chosen action was better than the expected/average action.
Therefore, The advantage has lower variance since the baseline compensates for the variance introduced
by being in different states. It is also worth noting that the baseline is independent of the policy gradient, thus
not changing the bias (expected value).

Specifically, an example where it helps, is when the spaceship turns-over. This is a state at which the expected reward
is very low, and typically most of the actions lead to the spaceship crashing, rather than landing safely. By subtracting
the baseline, we ensure that such states are harder too reach. When a spaceship is stable on it's axis, we want it
to stay stable and not turn-over, which it has some positive probability of doing. Consider a state of the spaceship
as the angle of it's position $\theta$, where it's probability of turning each angle along the axis is normally distributed
between $[-\frac{\pi}{2}, \frac{\pi}{2}]$. As the angle gets steeper, the probability of the spaceship turning should decrease.
Given a high-variance gaussian, the spaceship is more "prone to error", while lower variance ensures it stays on
smaller angles and therefore is more stable in it's descent.

...
"""


part1_q2 = r"""
**Your answer:**

In our model, we've defined the loss as follows:

$$
\begin{align}
\hat\grad\mathcal{L}_{\text{AAC}}(\vec{\theta})
=-\frac{1}{N}\sum_{i=1}^{N}\sum_{t\geq0} A\left(s_t,a_t\right) \grad\log \pi_{\vec{\theta}}(a_{i,t}|s_{i,t}).
\end{align}
$$

Where $A\left(s_t,a_t\right)$ is known as our Advantage function, and is defined as 
$A\left(s_t,a_t\right)=q_\pi(s_t,a_t) - v_\pi(s_t)$. of course, we've seen that $q$ (our action-value function) 
is usually unknown and therefore we use a neural network to estimate it.
Recall the definition of $v_\pi$ and $q_\pi$:

$$
\begin{align}
v_{\pi}(s) &= \E{g(\tau)|s_0 = s,\pi} \\
q_{\pi}(s,a) &= \E{g(\tau)|s_0 = s,a_0=a,\pi}.
\end{align}
$$

$v_\pi$ averages over the first action according to the policy, while $q_\pi$ fixes the first action and then 
continues according to the policy. Thus, we can say the $v_\pi$ is simply the expectation of $q_\pi$ over all 
possible starting actions $a_0$. This is a linear transformation, and therefore fits a regression problem, 
which is exactly what we are trying to solve (using MSE as our target loss for the state-values).

Therefore, we can estimate $A\left(s_t,a_t\right)$ by estimating only $q_\pi$.
"""


part1_q3 = r"""
**Answer:**

#### 1. Let's first examine the trends in the graphs:

1. For `vpg`, `epg`, as training progresses, the `loss_p` and `loss_e` tend to increase (maximize), and as we've seen in our
implementation, this is indicative of the model trying to maximize the reward.
2. For `cpg`, `bpg`, the `loss_p` tends to stay close to zero (with a small variance). Furthermore, their baseline
tends to increase as training progresses. This is due to the baseline policy reducing the variance (as we've explained)
and keeping the expected value **unchanged**.
3. For all models, the mean reward tends to increase as training progresses. This indicates a good learning progress,
as the model samples different batches and converges to a good maximum. The mean reward has a good learning rate and
tends to improve in a stable manner.

Generally, all model baselines have a lot of spikes in the graph. This is indicative of the high variance of the
baseline itself (recall that, for example, the baseline for `bpg` was the mean Q-value, which due to the complexity
of the game, can have largely different values).

Entropy regularization is commonly used to improve policy optimization in reinforcement learning, by allowing the
agent to take more "random" (stochastic) steps, rather than fixating on some strategy. Recall that entropy is a 
measure of uncertainty of the probability distribution. thus, when entropy is high, it allows a more "close to uniform"
behavior for the agent.

#### 2. AAC comparison

We notice that the mean reward tends to go up, same is with the other losses. `loss_p` values also tend to go up,
in a faster rate than other models, and finally converge to similar values. As for `loss_e`, we see that the loss value
is much lower than other losses. This is due to the `loss_v` (state-value loss) factor we've introduced in the 
actor-critic model. Compared to `cpg`, the `loss_p` values grow at a faster rate, because we've introduced another
component of value loss, and calculated our baseline as the state-value function we learn from the actor-critic model.
Thus, the gradient of the loss tends to progress faster in the direction of the maximum.


"""
